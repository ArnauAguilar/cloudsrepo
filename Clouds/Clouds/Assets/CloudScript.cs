﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class CloudScript : MonoBehaviour
{
    // Start is called before the first frame update
    Renderer display;
    public RenderTexture result;
    public ComputeShader shader;
    public ComputeShader viewShader;
    int kernel;
    int kernelView;
    public RenderTexture output;
    [Range(0,512 - 1)]
    public int depth;
    void Start()
    {
        display = GetComponent<Renderer>();
        kernel = shader.FindKernel("CSMain");
        kernelView = viewShader.FindKernel("CSMain");

        result = new RenderTexture(512, 512, 0,RenderTextureFormat.ARGB32);
        result.enableRandomWrite= true;
        result.dimension = UnityEngine.Rendering.TextureDimension.Tex3D;
        result.volumeDepth = 512;
        result.Create();

        output = new RenderTexture(512, 512, 0, RenderTextureFormat.ARGB32);
        output.enableRandomWrite= true;
        output.Create();

        display.material.mainTexture = output;


    }

    // Update is called once per frame
    void Update()
    {
        shader.SetTexture(kernel, "Result", result);

        viewShader.SetTexture(kernelView, "Result", output);
        viewShader.SetTexture(kernelView, "Data", result);
        viewShader.SetInt("z", depth);

        shader.Dispatch(kernel, 512 / 8, 512 / 8, 512 / 8);
        viewShader.Dispatch(kernelView, 512 / 8, 512 / 8, 1);



    }
}
